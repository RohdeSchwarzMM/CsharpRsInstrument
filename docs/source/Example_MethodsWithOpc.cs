.. code-block:: csharp

   // Write / Query with OPC
   // The SCPI commands syntax is for demonstration only
   
   using System;
   using System.Collections.Generic;
   using System.Linq;
   using System.Text;
   using RohdeSchwarz.RsInstrument;
   
   namespace Examples
   {
       class Program
       {
           static void Main()
           {
               RsInstrument instr = new RsInstrument("TCPIP::192.168.1.100::INSTR");
               instr.VisaTimeout = 3000;
               // OpcTimeout default value is 10000 ms
               instr.OpcTimeout = 20000;
   
               // Send Reset command and wait for it to finish
               instr.WriteStringWithOpc("*RST");
   
               // Initiate the measurement and wait for it to finish, define the timeout 50 secs
               // Notice no changing of the VISA timeout
               instr.WriteStringWithOpc("INIT", 50000);
               // The results are ready, simple fetch returns the results
               // Waiting here is not necessary
               var result1 = instr.QueryString("FETCH:MEASUREMENT?");
   
               // READ command starts the measurement,
               // We use QueryStringWithOpc to wait for the measurement to finish
               var result2 = instr.QueryStringWithOpc("READ:MEASUREMENT?", 50000);
   
               // Close the session
               instr.Dispose();
           }
       }
   }