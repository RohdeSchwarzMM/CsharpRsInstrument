.. code-block:: csharp

   // Two RsInstrument objects are used in multiple threads and are accessing the same instrument
   
   using System;
   using System.Collections.Generic;
   using System.Linq;
   using System.Text;
   using System.Threading.Tasks;
   using RohdeSchwarz.RsInstrument;
   
   namespace Examples
   {
       class Program
       {
           static void Main()
           {
               RsInstrument instr1 = new RsInstrument("TCPIP::192.168.1.100::INSTR");
               RsInstrument instr2 = new RsInstrument(instr1.Session);
               instr1.VisaTimeout = 200;
               // Synchronize the locks of both sessions
               instr2.AssignLock(instr1.GetLock());
   
               Parallel.For(1, 20,
                   x =>
                   {
                       if (x % 10 == 0)
                       {
                           // For each even x number, query the instr1
                           Console.WriteLine("Instr1 {0} started...", x);
                           var response = instr1.QueryString("*IDN?");
                           Console.WriteLine("...Instr1 Query {0} finished.", x);
                       }
                       else
                       {
                           // For each odd x number, query the instr2
                           Console.WriteLine("Instr2 {0} started...", x);
                           var response = instr2.QueryString("*IDN?");
                           Console.WriteLine("...Instr2 Query {0} finished.", x);
                       }
                   });
   
               Console.WriteLine("\nPress any key ...");
               Console.ReadKey();
   
               // Close the sessions
               instr2.Dispose();
               instr1.Dispose();
           }
       }
   }