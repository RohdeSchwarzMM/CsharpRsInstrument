.. code-block:: csharp

   // Hello World example for any R&S Instrument
   
   using System;
   using System.Collections.Generic;
   using System.Linq;
   using System.Text;
   using System.Threading.Tasks;
   using RohdeSchwarz.RsInstrument;
   
   namespace Examples
   {
       class Program
       {
           static void Main()
           {
               // A good practice is to assure that you have a certain minimum version installed
               RsInstrument.AssertMinVersion("1.23.0");
               // Standard LAN connection (also called VXI-11)
               var resourceString1 = "TCPIP::192.168.1.100::INSTR";
               // Hi-Speed LAN connection - see 1MA208
               var resourceString2 = "TCPIP::192.168.1.100::hislip0";
               // GPIB Connection
               var resourceString3 = "GPIB::20::INSTR";
               // USB-TMC (Test and Measurement Class)
               var resourceString4 = "USB::0x0AAD::0x0119::022019943::INSTR";
               // R&S Powersensor NRP-Z86 (needs NRP-Toolkit installed)
               var resourceString5 = "RSNRP::0x0095::104015::INSTR";
   
               // Initializing the session
               RsInstrument instr = new RsInstrument(resourceString1);
   
               Console.WriteLine("RsInstrument Driver Version: " + instr.Identification.DriverVersion);
               Console.WriteLine("Visa Manufacturer: " + instr.Identification.VisaManufacturer);
               Console.WriteLine("Instrument Full Name: " + instr.Identification.InstrumentFullName);
               Console.WriteLine("Installed Options: " + string.Join(",", instr.Identification.InstrumentOptions));
               string idn = instr.QueryString("*IDN?");
               Console.WriteLine("\nHello, I am: " + idn);
   
               Console.WriteLine("\nPress any key ...");
               Console.ReadKey();
   
               // Close the session
               instr.Dispose();
           }
       }
   }