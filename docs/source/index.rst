.. RsInstrument documentation master file, created by
   sphinx-quickstart on Tue Jan  5 12:17:17 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to RsInstrument's C# documentation!
============================================

.. image:: icon.png
   :class: with-shadow
   :align: right
   
RsInstrument is a .NET component that provides convenient way of communicating with Rohde & Schwarz instruments.

Basic Hello-World code:

.. include:: Example_Intro.cs

More examples on Rohde & Schwarz Github repository:
   - `Miscelaneous C# <https://github.com/Rohde-Schwarz/Examples/tree/main/Misc/Csharp/RsInstrument>`_
   - `Miscelaneous VB.NET <https://github.com/Rohde-Schwarz/Examples/tree/main/Misc/VB.NET/RsInstrument>`_
   - `Oscilloscopes <https://github.com/Rohde-Schwarz/Examples/tree/main/Oscilloscopes/Csharp/RsInstrument>`_
   - `Powersensors <https://github.com/Rohde-Schwarz/Examples/tree/main/Powersensors/Csharp/RsInstrument>`_
   - `Spectrum Analyzers <https://github.com/Rohde-Schwarz/Examples/tree/main/SpectrumAnalyzers/Csharp/RsInstrument>`_

Preconditions
   - Installed `R&S VISA 5.12+ <https://www.rohde-schwarz.com/appnote/1dc02>`_ or NI VISA 18.0+
   - No VISA installation is necessary if you select the plugin SocketIO

Supported Frameworks
   - .NET Core 3.1
   - .NET Standard 2.1
   - .NET Standard 2.0
   - .NET Framework 4.8
   - .NET Framework 4.5

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   Readme
   StepByStepGuide


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
