13. Transferring Big Data with Progress
========================================

We can agree that it can be annoying using an application that shows no progress for long-lasting operations. The same is true for remote-control programs. Luckily, the RsInstrument has this covered. And, this feature is quite universal - not just for big files transfer, but for any data in both directions.

RsInstrument allows you to register a function (programmers fancy name is '*callback*'), which it invokes after each transfer of one data chunk. Moreover, because you can set the chunk size, you can pace it as you desire. You can even slow down the transfer speed, if you want to process the data as they arrive (direction instrument -> PC).
To show this in praxis, we are going to use another *University-Professor-Example*: querying the **\*IDN?** with chunk size of 2 bytes and delay of 200ms between each chunk read:

.. include:: Example_QueryWithProgress.cs

If you start it, you might wonder (or maybe not): why is the ``args.TotalSize = None``? The reason is, in this particular case the RsInstrument does not know the size of the complete response up-front. However, if you use the same mechanism for transfer of a known data size (for example, a file transfer), you get the information about the total size too, and hence you can calculate the progress as:

*progress [pct] = 100 \* args.TransferredSize / args.TotalSize*

The following example transfers a file from instrument to the PC and back. In the callback, we skip showing the data, since it's a quite big binary stream. Depending on the file size, adjust the ``IoSegmentSize`` and the ``Thread.Sleep()`` delay in the callback:

.. include:: Example_WriteReadFileWithProgress.cs
