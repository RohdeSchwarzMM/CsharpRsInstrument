12. Transferring Files
========================================

Instrument -> PC
""""""""""""""""""""""""""""""""""""""""""""""""""""
You just did a perfect measurement, saved the results as a screenshot to the instrument's storage drive.
Now you want to transfer it to your PC.
With RsInstrument, no problem, just figure out where the screenshot was stored on the instrument. In our case, it is ``var/user/instr_screenshot.png``:

.. code-block:: csharp

    instr.File.FromInstrumentToPc(@"var/user/instr_screenshot.png", @"c:\temp\pc_screenshot.png");

PC -> Instrument
""""""""""""""""""""""""""""""""""""""""""""""""""""
Another common scenario: Your cool test program contains a setup file you want to transfer to your instrument:
Here is the RsInstrument one-liner:

.. code-block:: csharp
    
    instr.File.FromPcToInstrument(@"c:\MyCoolTestProgram\instr_setup.sav", @"var/appdata/instr_setup.sav");
