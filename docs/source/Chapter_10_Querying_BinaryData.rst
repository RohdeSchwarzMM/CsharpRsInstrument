10. Querying Binary Data
========================================

A common question from customers: How do I read binary data to a byte stream, or a file?

If you want to transfer files between PC and your instrument, check out the following chapter: :doc:`12_Transferring_Files <Chapter_12_TransferringFiles>`.

Querying to a Byte Array
""""""""""""""""""""""""""""""""""""""""""""""""""""
Let us say you want to get raw RTO waveform data. Call this method:

.. code-block:: csharp
    
    byte[] data = instr.Binary.QueryData("FORM REAL,32;:CHAN1:DATA?");
    
Querying to PC files
""""""""""""""""""""""""""""""""""""""""""""""""""""
Modern instrument can acquire gigabytes of data, which is often more than your program can hold in memory. The solution may be to save this data to a file. RsInstrument is smart enough to read big data in chunks, which it immediately writes into a file stream. This way, at any given moment your program only holds one chunk of data in memory. You can set the chunk size with the property ``IoSegmentSize``. The initial value is 100 000 bytes.

We are going to read the RTO waveform into a PC file *c:\\temp\\rto_waveform_data.bin*:

.. code-block:: csharp
    
    rto.IoSegmentSize = 100000;
    var myPcFile = new FileStream(@"c:\temp\rto_waveform_data.bin", FileMode.OpenOrCreate);
    instr.Binary.QueryData("FORM REAL,32;:CHAN1:DATA?", myPcFile);
