.. code-block:: csharp

   // Find any USB instruments in your environment with the VISA preference
   
   using System;
   using System.Collections.Generic;
   using System.Linq;
   // Install as NuGet package from www.nuget.org
   using RohdeSchwarz.RsInstrument;
   
   namespace Examples
   {
       class Program
       {
           static void Main()
           {
               // 'vxi11' and 'lxi' boolean arguments have only effect for R&S VISA
               // As 'plugin' you can use:
               // - 'NativeVisa' (default system VISA)
               // - 'RsVisa' (Rohde & Schwarz VISA)
               // - 'RsVisaPrio' (Prefer Rohde & Schwarz VISA, fall back to default VISA)
               string plugin = "RsVisa";
               var instrList = RsInstrument.FindResources("USB?*", false, false, plugin);
               instrList.ToList().ForEach(Console.WriteLine);
           }
       }
   }