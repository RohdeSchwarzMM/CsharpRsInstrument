.. code-block:: csharp

   // Using RsInstrument without VISA for LAN Raw socket communication
   
   using System;
   using System.Collections.Generic;
   using System.Linq;
   using System.Text;
   using RohdeSchwarz.RsInstrument;
   
   namespace Examples
   {
       class Program
       {
           static void Main()
           {
               var resourceString = "TCPIP::192.168.1.100::5025::SOCKET";  // SOCKET-like resource name
               RsInstrument instr = new RsInstrument(resourceString, true, false, "SelectVisa=SocketIo");
               string idn = instr.QueryString("*IDN?");
               Console.WriteLine("\nHello, I am: " + idn);
   
               Console.WriteLine("\nNo VISA has been harmed or even used in this example.");
               Console.WriteLine("Press any key ...");
               Console.ReadKey();
   
               // Close the session
               instr.Dispose();
           }
       }
   }