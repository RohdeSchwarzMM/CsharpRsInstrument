11. Writing Binary Data
========================================

Writing from bytes data
""""""""""""""""""""""""""""""""""""""""""""""""""""
We take example for a Signal generator waveform data file. First, we construct a ``waveformData`` as ``byte[]``, and then send it with ``WriteData()``:

.. code-block:: csharp

    // MyWaveform.wv is an instrument file name under which this data is stored
    smw.Binary.WriteData("SOUR:BB:ARB:WAV:DATA 'MyWaveform.wv',", waveformData);

Notice the ``WriteData()`` has two parameters:

- ``string`` parameter ``command`` for the SCPI command
- ``byte[]`` parameter ``binaryData`` for the payload data

Writing from PC files
""""""""""""""""""""""""""""""""""""""""""""""""""""
The ``WriteData()`` has two overloads - either you provide data as ``byte[]`` or as a ``Stream``, for example a ``FileStream`` connected to your source PC file:

.. code-block:: csharp

    FileStream mySourcePcFile = new FileStream(@"c:\temp\rto_waveform_data.bin", FileMode.Open);
    smw.Binary.WriteData("SOUR:BB:ARB:WAV:DATA 'MyWaveform.wv',", mySourcePcFile);
