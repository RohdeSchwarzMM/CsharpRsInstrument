.. code-block:: csharp

   // Basic string WriteString / QueryString
   
   using System;
   using System.Collections.Generic;
   using System.Linq;
   using System.Text;
   using System.Threading.Tasks;
   using RohdeSchwarz.RsInstrument;
   
   namespace Examples
   {
       class Program
       {
           static void Main()
           {
               RsInstrument instr = new RsInstrument("TCPIP::192.168.1.100::INSTR");
               instr.Reset();
               Console.WriteLine(instr.Identification.IdnString);
   
               Console.WriteLine("\nPress any key ...");
               Console.ReadKey();
   
               // Close the session
               instr.Dispose();
           }
       }
   }