.. code-block:: csharp

   // How to deal with RsInstrument exceptions
   
   using System;
   using System.Collections.Generic;
   using System.Linq;
   using System.Text;
   using System.Threading.Tasks;
   using RohdeSchwarz.RsInstrument;
   
   namespace Examples
   {
       class Program
       {
           static void Main()
           {
               RsInstrument instr;
               try // Separate try-catch for initialization prevents accessing uninitialized object
               {
                   // Adjust the VISA Resource string to fit your instrument
                   instr = new RsInstrument("TCPIP::192.168.1.100::INSTR");
               }
               catch (RsInstrumentException e)
               {
                   Console.WriteLine(e.Message);
                   Console.WriteLine("Your instrument is probably OFF...");
                   // Exit now, no point of continuing
                   Console.WriteLine("Press any key to finish.");
                   Console.ReadKey();
                   return;
               }
   
               // Dealing with commands that potentially generate errors OPTION 1:
               // Switching the status checking OFF temporarily
               instr.InstrumentStatusChecking = false;
               instr.WriteString("MY:MISSpelled:COMMand");
               // Clear the error queue
               instr.ClearStatus();
               // Status checking ON again
               instr.InstrumentStatusChecking = true;
   
               // Dealing with queries that potentially generate errors OPTION 2:
               try
               {
                   instr.VisaTimeout = 1000;
                   instr.QueryString("MY:OTHEr:WRONg:QUERy?");
               }
               catch (InstrumentStatusException e)
               {
                   // Instrument status error
                   Console.WriteLine(e.Message);
                   Console.WriteLine("Nothing to see here, moving on...");
               }
               catch (VisaException e)
               {
                   // General Visa error
                   Console.WriteLine(e.Message);
                   Console.WriteLine("Somethin's seriously wrong...");
               }
               catch (VisaTimeoutException e)
               {
                   // Visa Timeout error
                   Console.WriteLine(e.Message);
                   Console.WriteLine("That took a long time... longer than the VISA timeout");
               }
               catch (OperationTimeoutException e)
               {
                   // OPC Timeout error
                   Console.WriteLine(e.Message);
                   Console.WriteLine("You called some method with OPC, and that took a long time...");
               }
               catch (RsInstrumentException e)
               {
                   // General RsInstrument error.
                   // RsInstrumentException is a base class for all the RsInstrument exceptions
                   Console.WriteLine(e.Message);
                   Console.WriteLine("Some other RsInstrument error...");
               }
               finally
               {
                   instr.VisaTimeout = 5000;
                   // Close the session in any case
                   instr.Dispose();
               }
           }
       }
   }