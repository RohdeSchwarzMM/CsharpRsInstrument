1. Introduction
========================================

.. image:: icon.png
   :class: with-shadow
   :align: left

RsInstrument is a C# remote-control communication module for Rohde & Schwarz SCPI-based Test and Measurement Instruments.
The original title of this document was **"10 Tips and Tricks..."**, but there were just too many cool features to fit into 10 chapters. We wanted to skip thirteen, so we ended up with fourteen.

Some of the RsInstrument's key features:

- You can select which VISA to use or even not use any VISA at all
- Initialization of a new session is straight-forward, no need to set any other properties
- Many useful features are already implemented - reset, self-test, opc-synchronization, error checking, option checking
- Binary data blocks transfer in both directions
- Transfer of arrays of numbers in binary or ASCII format
- File transfers in both directions
- Events generation in case of error, sent data, received data, chunk data (in case of big data transfer)
- Multithreading session locking - you can use multiple threads talking to one instrument at the same time
