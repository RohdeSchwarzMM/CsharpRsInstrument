3. Finding available instruments
========================================

RsInstrument can search for available instruments:

.. include:: Example_FindInstrument.cs

If you have more VISAs installed, the one actually used by default is defined by a secret widget called VISA Conflict Manager. You can force your program to use R&S VISA:

.. include:: Example_FindInstrumentSelectVisa.cs

.. tip::
    We believe our R&S VISA is the best choice for our customers. Here are the reasons why:
    
    - Small footprint
    - Superior VXI-11 and HiSLIP performance
    - Integrated legacy sensors NRP-Zxx support
    - Additional VXI-11 and LXI devices search
    - Availability for Windows, Linux, Mac OS
