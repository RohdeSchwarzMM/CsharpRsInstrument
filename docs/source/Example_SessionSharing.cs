.. code-block:: csharp

   // Sharing the same physical VISA session by two different RsInstrument objects
   
   using System;
   using System.Collections.Generic;
   using System.Linq;
   using System.Text;
   using System.Threading.Tasks;
   using RohdeSchwarz.RsInstrument;
   
   namespace Examples
   {
       class Program
       {
           static void Main()
           {
               var instr1 = new RsInstrument("TCPIP::192.168.1.100::INSTR");
               var instr2 = new RsInstrument(instr1.Session);
   
               Console.WriteLine("instr1: " + instr1.Identification.IdnString);
               Console.WriteLine("instr2: " + instr2.Identification.IdnString);
   
               // Closing the instr2 session does not close the instr1 session.
               // instr1 is the 'session master'
               instr2.Dispose();
               Console.WriteLine("instr2: I am closed now");
   
               Console.WriteLine("instr1: I am  still opened and working.");
               Console.WriteLine("Look, I can prove it: " + instr1.Identification.IdnString);
   
               instr1.Dispose();
               Console.WriteLine("instr1: Only now I am closed.");
   
               Console.WriteLine("\nPress any key ...");
               Console.ReadKey();
           }
       }
   }