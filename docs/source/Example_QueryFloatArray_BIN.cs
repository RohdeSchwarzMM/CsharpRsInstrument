.. code-block:: csharp

   // Querying binary float arrays
   
   using System;
   using System.Collections.Generic;
   using System.Linq;
   using System.Text;
   using System.Threading.Tasks;
   using RohdeSchwarz.RsInstrument;
   
   namespace Examples
   {
       class Program
       {
           static void Main()
           {
               RsInstrument rto = new RsInstrument("TCPIP::192.168.1.100::INSTR", true, true);
   
               // Initiate a single acquisition and wait for it to finish
               rto.WriteStringWithOpc("SINGle", 20000);
   
               // This tells the RsInstrument in which format to expect the binary float data
               rto.Binary.FloatNumbersFormat = InstrBinaryFloatNumbersFormat.Single4Bytes;
               // If your instrument sends the data with the swapped endianness, use the following value:
               // rto.Binary.FloatNumbersFormat = InstrBinaryFloatNumbersFormat.Single4BytesSwapped;
   
               double[] waveform = rto.Binary.QueryBinOrAsciiFloatArray("FORM REAL,32;:CHAN1:DATA?");
               Console.WriteLine("Instrument returned points: " + waveform.Length);
   
               // Close the session
               rto.Dispose();
           }
       }
   }