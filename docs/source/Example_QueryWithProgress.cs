.. code-block:: csharp

   // Event handlers by reading - querying string data
   
   using System;
   using System.Collections.Generic;
   using System.IO;
   using System.Linq;
   using System.Text;
   using System.Threading;
   using System.Threading.Tasks;
   using RohdeSchwarz.RsInstrument;
   
   namespace Examples
   {
       class Program
       {
           static void MyTransferHandler(object sender, InstrSegmentEventArgs args)
           {
               Console.Write("\nSegment: " + args.SegmentIx);
               Console.Write(", transferred bytes: " + args.TransferredSize);
               Console.Write(", total size: " + args.TotalSize);
   
               // Get the current segment data
               args.DataStream.Position = args.SegmentDataOffset;
               string text = new StreamReader(args.DataStream).ReadToEnd();
               Console.Write(", data: '" + text + "'");
               args.DataStream.Position = args.DataStream.Length;
   
               if (args.Finished)
               {
                   Console.WriteLine("\n\nEnd of Transfer");
               }
               else
               {
                   // Pause here for 100ms after each segment read
                   Thread.Sleep(100);
               }
           }
   
           static void Main()
           {
               RsInstrument instr = new RsInstrument("TCPIP::192.168.1.100::INSTR");
               instr.Events.ReadSegmentHandler = MyTransferHandler;
               // Set data chunk size to 2 bytes
               instr.IoSegmentSize = 2;
               var response = instr.QueryString("*IDN?");
               instr.Events.ReadSegmentHandler = null;
   
               // Close the session
               instr.Dispose();
           }
       }
   }