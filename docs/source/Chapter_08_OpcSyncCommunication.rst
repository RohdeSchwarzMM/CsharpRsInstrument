8. OPC-synchronized I/O Communication
========================================

Now we are getting to the cool stuff: OPC-synchronized communication. OPC stands for OPeration Completed. The idea is: use one method (write or query), which sends the command, and polls the instrument's status subsystem until it indicates: **"I'm finished"**. The main advantage is, you can use this mechanism for commands that take several seconds, or minutes to complete, and you are still able to interrupt it if needed. You can also perform other operations with the instrument in a parallel thread.

Now, you might say: **"This sounds complicated, I'll never use it"**. That is where the RsInstrument comes in: all the **Write/Query** methods we learned in the previous chapter have their ``WithOpc`` siblings. For example: ``WriteString()`` has ``WriteStringWithOpc()``. You can use them just like the normal write/query with one difference: They all have an optional parameter ``timeout``, where you define the maximum time to wait. If you omit it, it uses value from ``OpcTimeout`` property.
Important difference between the meaning of ``VisaTimeout`` and ``OpcTimeout``:

- ``VisaTimeout`` is a VISA IO communication timeout. **It does not play any role in the** ``WithOpc()`` methods. It only defines timeout for the standard ``QueryXxx()`` methods. We recommend to keep it to maximum of 10000 ms.
- ``OpcTimeout`` is a RsInstrument internal timeout, that serves as a default value to all the ``WithOpc()`` methods. If you explicitly define it in the method API, it is valid only for that one method call.

That was too much theory... Now an example:

.. include:: Example_MethodsWithOpc.cs
