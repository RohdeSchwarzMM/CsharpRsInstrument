7. Exception handling
========================================
The base class for all the exceptions thrown by the RsInstrument is ``RsInstrumentException``. Inherited exception classes:

- ``InstrumentStatusException`` thrown if a command or a query generated error in the instrument's error queue
- ``VisaException`` thrown if the underlying VISA component throws an error
- ``OperationTimeoutException`` thrown if OPC timeout is reached
- ``VisaTimeoutException`` thrown if visa timeout is reached

In this example we show usage of all of them:

.. include:: Example_Exceptions.cs

.. tip::
    General rules for exception handling:

    - If you are sending commands that might generate errors in the instrument, for example deleting a file which does not exist, use the **OPTION 1** - temporarily disable status checking, send the command, clear the error queue and enable the status checking again.
    - If you are sending queries that might generate errors or timeouts, for example querying measurement that cannot be performed at the moment, use the **OPTION 2** - try/catch with optionally adjusting timeouts.