2. Installation
========================================

RsInstrument is hosted on  `NuGet.org <https://www.nuget.org/packages?q=rsinstrument>`_. You can install it with **Visual Studio Package Manager** or **Visual Studio Packet Manager Console**:

.. code-block:: console

    PM> Update-Package -Id RsInstrument –reinstall
