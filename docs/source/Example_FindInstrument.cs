.. code-block:: csharp

   // Find the instruments in your environment
   
   using System;
   using System.Collections.Generic;
   using System.Linq;
   // Install as NuGet package from www.nuget.org repository
   using RohdeSchwarz.RsInstrument;
   
   namespace Examples
   {
       class Program
       {
           static void Main()
           {
               // Use the instrList items later as resource names in the RsInstrument constructor
               IEnumerable<string> instrList = RsInstrument.FindResources("?*");
               instrList.ToList().ForEach(Console.WriteLine);
           }
       }
   }