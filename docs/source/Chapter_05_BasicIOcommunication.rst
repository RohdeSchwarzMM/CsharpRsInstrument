5. Basic I/O communication
========================================

Now we have opened the session, it's time to do some work. RsInstrument provides two basic methods for communication:

- ``WriteString()`` - writing a command without an answer e.g.: **\*RST**
- ``QueryString()`` - querying your instrument, for example with the **\*IDN?** query

You may ask a question. Actually, two questions:

- **Q1**: Why there are not called ``Write()`` and ``Query()`` ?
- **Q2**: Where is the ``Read()`` ?

**Answer 1**: Actually, there are: ``Write()`` = ``WriteString()`` and ``Query()`` = ``QueryString()``, they are aliases. To avoid mixing string and binary communication, we promote the ones with ``String`` names. All the method names for binary transfer contain ``Bin`` in their names.

**Answer 2**: Short answer - you do not need it. Long answer - your instrument never sends unsolicited responses. If you send a set-command, you use ``WriteString()``. For a query-command, you use ``QueryString()``. So, you really do not need it...

Enough with the theory, let us look at an example. Simple write and query:

.. include:: Example_SimpeStringIoUniversity.cs

This example is so-called "*University-Professor-Example*" - good to show a principle, but never used in praxis. The abovementioned commands are already a part of the driver's API. Here is another example, achieving the same goal:

.. include:: Example_SimpeStringIo.cs

One additional feature we need to mention here: **VISA Timeout**. To simplify, VISA Timeout plays a role in each ``QueryXxx()``, where the controller (your PC) has to prevent waiting forever for an answer from your instrument. VISA Timeout defines that maximum waiting time. You can set/read it with the ``VisaTimeout`` property:

.. code-block:: csharp

    // Timeout in milliseconds
    instr.VisaTimeout = 3000;

After this time, RsInstrument raises an exception. Speaking of exceptions, an important feature of the RsInstrument is **Instrument Status Checking**. Check out the next chapter that describes the error checking in details.

.. tip::
    If you're dying for the ``Write()`` and ``Query()``, use the cool extensions feature of C#:

    .. code-block:: csharp

        public static class RsInstrumentExtensions
        {
            // Mapping Write() to WriteString()
            public static void Write(this RsInstrument instr, string command)
            {
                instr.WriteString(command);
            }

            // Mapping Query() to QueryString()
            public static string Query(this RsInstrument instr, string query)
            {
                return instr.QueryString(query);
            }
        }

For completion, we mention other string-based ``QueryXxx()`` methods, all in one example. They are convenient extensions providing type-safe double/boolean/integer querying features:

.. include:: Example_SimpleStringIoOtherTypes.cs

Lastly, a method providing basic synchronization: ``QueryOpc()``. It sends query **\*OPC?** to your instrument. The instrument waits with the answer until all the tasks it currently has in the execution queue are finished. This way your program waits too, and it is synchronized with actions in the instrument. Remember to have the VISA timeout set to an appropriate value to prevent the timeout exception. Here's a snippet:

.. code-block:: csharp

    var oldTout = instr.VisaTimeout;
	instr.VisaTimeout = 3000;
    instr.WriteString("INIT");
    instr.QueryOpc();
	instr.VisaTimeout = oldTout;
    
    // The results are ready now to fetch
    results = instr.QueryString("FETCH:MEASUREMENT?");

You can also specify the ``VisaTimeout`` just for one ``QueryOpc()`` call. The following example is equivalent to the one above:

.. code-block:: csharp

    instr.WriteString("INIT");
	// Set the VISA Timeout for this call only, then set it back to the original value
    instr.QueryOpc(3000);
    
    // The results are ready now to fetch
    results = instr.QueryString("FETCH:MEASUREMENT?");

.. tip::
    Wait, there's more: you can send the **\*OPC?** after each ``WriteString()`` automatically:

    .. code-block:: csharp

        // Default value after init is false
        instr.OpcQueryAfterEachSetting = true;
