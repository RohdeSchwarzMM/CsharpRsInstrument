.. code-block:: csharp

   // Querying ASCII and binary integer arrays
   
   using System;
   using System.Collections.Generic;
   using System.Linq;
   using System.Text;
   using System.Threading.Tasks;
   using RohdeSchwarz.RsInstrument;
   
   namespace Examples
   {
       class Program
       {
           static void Main()
           {
               RsInstrument rto = new RsInstrument("TCPIP::192.168.1.100::INSTR", true, true);
   
               // Initiate a single acquisition and wait for it to finish
               rto.WriteStringWithOpc("SINGle", 20000);
   
               // Query array of integers in ASCII format
               int[] waveform = rto.Binary.QueryBinOrAsciiIntegerArray("FORM ASC;:CHAN1:DATA?");
               Console.WriteLine("Instrument returned points: " + waveform.Length);
   
               // Query array of integers in Binary format
               // This tells the RsInstrument in which format to expect the binary integer data
               rto.Binary.IntegerNumbersFormat = InstrBinaryIntegerNumbersFormat.Integer324Bytes;
               // If your instrument sends the data with the swapped endianness, use the following value:
               //rto.Binary.IntegerNumbersFormat = InstrBinaryIntegerNumbersFormat.Integer324BytesSwapped;
               waveform = rto.Binary.QueryBinOrAsciiIntegerArray("FORM INT,32;:CHAN1:DATA?");
               Console.WriteLine("Instrument returned points: " + waveform.Length);
   
               // Close the session
               rto.Dispose();
           }
       }
   }