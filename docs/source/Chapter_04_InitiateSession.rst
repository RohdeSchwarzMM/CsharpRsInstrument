4. Initiating instrument session
========================================

RsInstrument offers four different types of starting your remote-control session. We begin with the most typical case, and progress with more special ones.

Standard Session Initialization
""""""""""""""""""""""""""""""""""""""""""""""""""""
Initiating new instrument session happens, when you instantiate the RsInstrument object. Below, is a Hello World example. Different resource names are examples for different physical interfaces.

.. include:: Example_HelloWorld.cs

.. note::
    If you are wondering about the missing ``ASRL1::INSTR``, yes, it works too, but come on... it's 2024 :-).

Do not care about specialty of each session kind; RsInstrument handles all the necessary session settings for you. You immediately have access to many identification properties. Here are same of them:

.. code-block:: csharp

	string instr.Identification.IdnString;
	Version instr.Identification.DriverVersion;
	string instr.Identification.VisaManufacturer;
	string instr.Identification.InstrumentFullName;
	string instr.Identification.InstrumentSerialNumber;
	string instr.Identification.InstrumentFirmwareVersion;
	List<string> instr.Identification.InstrumentOptions;

The constructor also contains optional boolean arguments ``idQuery`` and ``resetDevice``:

.. code-block:: csharp

    bool idQuery = true;
    bool resetDevice = true;
    RsInstrument instr = new RsInstrument("TCPIP::192.168.56.101::hislip0", idQuery, resetDevice);

- Setting ``idQuery`` to true (default is true) checks, whether your instrument can be used with the RsInstrument module.
- Setting  ``resetDevice`` to true (default is false) resets your instrument. It is equivalent to calling the ``Reset()`` method.

Selecting Specific VISA
""""""""""""""""""""""""""""""""""""""""""""""""""""
Just like in the static method ``FindResources()``, RsInstrument allows you to prefer R&S VISA:

.. include:: Example_DifferentVisas.cs

No VISA Session
""""""""""""""""""""""""""""""""""""""""""""""""""""
We recommend using VISA whenever possible, preferably with HiSlip session because of its low latency. However, if you are a strict VISA denier, RsInstrument has something for you too - **no Visa installation raw LAN socket**:

.. include:: Example_NoVisaSession.cs

.. warning::
    Not using VISA can cause problems by debugging when you want to use the communication Trace Tool. The good news is, you can easily switch to use VISA and back just by changing the constructor arguments. The rest of your code stays unchanged.

Simulating Session
""""""""""""""""""""""""""""""""""""""""""""""""""""
If a colleague is currently occupying your instrument, leave him in peace, and open a simulating session:

.. code-block:: csharp

    var instr = new RsInstrument("TCPIP::192.168.56.101::HISLIP", true, false, "Simulate=True");

More ``optionString`` tokens are separated by comma:

.. code-block:: csharp

    var optionString = "SelectVisa='rs', Simulate=True";
    var instr = new RsInstrument("TCPIP::192.168.56.101::HISLIP", true, false, optionString);

Shared Session
""""""""""""""""""""""""""""""""""""""""""""""""""""
In some scenarios, you want to have two independent objects talking to the same instrument. Rather than opening a second VISA connection, share the same one between two or more RsInstrument objects:

.. include:: Example_SessionSharing.cs

.. note::
    The ``instr1`` is the object holding the 'master' session. If you call the instr1.Dispose(), the ``instr2`` loses its instrument session as well, and becomes pretty much useless.