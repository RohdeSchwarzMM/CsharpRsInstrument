.. code-block:: csharp

   // Multiple threads are accessing one RsInstrument object
   
   using System;
   using System.Collections.Generic;
   using System.Linq;
   using System.Text;
   using System.Threading.Tasks;
   using RohdeSchwarz.RsInstrument;
   
   namespace Examples
   {
       class Program
       {
           static void Main()
           {
               RsInstrument instr = new RsInstrument("TCPIP::192.168.1.100::INSTR");
   
               Parallel.For(1, 10,
                   x =>
                   {
                       Console.WriteLine("Query {0} started...", x);
                       var response = instr.QueryString("*IDN?");
                       Console.WriteLine("...Query {0} finished.", x);
                   });
   
               Console.WriteLine("\nPress any key ...");
               Console.ReadKey();
   
               // Close the session
               instr.Dispose();
           }
       }
   }