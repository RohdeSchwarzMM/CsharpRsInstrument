14. Multithreading
========================================

You are at the party, many people talking over each other. Not every person can deal with such crosstalk, neither can measurement instruments. For this reason, RsInstrument has a feature of scheduling the access to your instrument by using so-called **Locks**. Locks make sure that there can be just one client at a time 'talking' to your instrument. Talking in this context means completing one communication step - one command write or write/read or write/read/error check.

To describe how it works, and where it matters, we take two typical multithread scenarios:

One instrument session, accessed from multiple threads
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
You are all set - the lock is a part of your instrument session. Check out the following example - it executes properly, although the instrument might get 10 queries at the same time:

.. include:: Example_MultithreadOneSession.cs

More instrument sessions, accessed from multiple threads
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
We have two objects, ``instr1`` and ``instr2`` talking to the same instrument from multiple threads. The ``instr1`` and ``instr2`` objects have two independent locks. We bring them in sync with this call:

.. code-block:: csharp

    instr2.AssignLock(instr1.GetLock());

Check out the following example:

.. include:: Example_MultithreadSharedSession.cs

.. note::
    If you want to simulate some party crosstalk, delete this line:
    
    .. code-block:: csharp

        instr2.AssignLock(instr1.GetLock());

    Although the ``instr1`` will still schedule its instrument access, the ``instr2`` will be doing the same at the same time, which will then lead to all that fun stuff happening.