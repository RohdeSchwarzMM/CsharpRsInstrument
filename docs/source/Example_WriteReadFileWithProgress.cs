.. code-block:: csharp

   // Event handlers by reading - writing and reading big files
   
   using System;
   using System.Collections.Generic;
   using System.IO;
   using System.Linq;
   using System.Text;
   using System.Threading;
   using System.Threading.Tasks;
   using RohdeSchwarz.RsInstrument;
   
   namespace Examples
   {
       class Program
       {
           static void MyRwHandler(object sender, InstrSegmentEventArgs args)
           {
               Console.Write("\nSegment: " + args.SegmentIx);
               Console.Write(", transferred bytes: " + args.TransferredSize);
               Console.Write(", total size: " + args.TotalSize);
   
               if (args.Finished)
               {
                   Console.WriteLine("\n\nEnd of Transfer");
               }
               else
               {
                   // Pause here for 1ms after each segment read
                   Thread.Sleep(1);
               }
           }
   
           static void Main()
           {
               RsInstrument instr = new RsInstrument("TCPIP::192.168.1.100::INSTR");
               // Set data chunk size to 100 000 bytes
               instr.IoSegmentSize = 100000;
               string pcFile = @"c:\temp\bigFile.bin";
               string pcFileBack = @"c:\temp\bigFileBack.bin";
               string instrFile = @"/var/user/bigFile.bin";
   
               Console.WriteLine("\n\nTransferring file from PC to the Instrument");
   
               instr.Events.WriteSegmentHandler = MyRwHandler;
               instr.File.FromPcToInstrument(pcFile, instrFile);
               instr.Events.WriteSegmentHandler = null;
   
               Console.WriteLine("\n\nTransferring the file back from Instrument to the PC");
   
               instr.Events.ReadSegmentHandler = MyRwHandler;
               instr.File.FromInstrumentToPc(instrFile, pcFileBack);
               instr.Events.ReadSegmentHandler = null;
   
               // Close the session
               instr.Dispose();
           }
       }
   }