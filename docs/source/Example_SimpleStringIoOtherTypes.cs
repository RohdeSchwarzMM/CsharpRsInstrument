.. code-block:: csharp

   // Other query methods
   
   using System;
   using System.Collections.Generic;
   using System.Linq;
   using System.Text;
   using System.Threading.Tasks;
   using RohdeSchwarz.RsInstrument;
   
   namespace Examples
   {
       class Program
       {
           static void Main()
           {
               RsInstrument instr = new RsInstrument("TCPIP::192.168.1.100::INSTR");
               instr.VisaTimeout = 5000;
               instr.InstrumentStatusChecking = true; // Default is true
               int scount = instr.QueryInteger("SWEEP:COUNT?"); // returning integer number
               bool output = instr.QueryBool("SOURCE:RF:OUTPUT:STATE?");  // returning boolean value
               double freq = instr.QueryDouble("SOURCE:RF:FREQUENCY?");  // returning float number
   
               // Close the session
               instr.Dispose();
           }
       }
   }