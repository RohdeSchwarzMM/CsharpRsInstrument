.. code-block:: csharp

   using System;
   using System.Text;
   using RohdeSchwarz.RsInstrument;
   
   namespace Examples
   {
       class Program
       {
           static void Main()
           {
               RsInstrument instr = new RsInstrument("TCPIP::192.168.1.122::hislip0");
               string idn = instr.Query("*IDN?");
               Console.WriteLine("\nHello, I am: " + idn);
               instr.Dispose();

               Console.WriteLine("\nPress any key ...");
               Console.ReadKey();
           }
       }
   }