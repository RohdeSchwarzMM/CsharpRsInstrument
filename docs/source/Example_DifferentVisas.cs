.. code-block:: csharp

   // Open your instrument with preferred Rohde & Schwarz VISA
   
   using System;
   using System.Collections.Generic;
   using System.Linq;
   using System.Text;
   using System.Threading.Tasks;
   using RohdeSchwarz.RsInstrument;
   
   namespace Examples
   {
       class Program
       {
           static void Main()
           {
               var resourceString = "TCPIP::192.168.1.100::INSTR";
               // Force use of the Rs Visa. For default system VISA, use the "SelectVisa=NativeVisa"
               RsInstrument instr = new RsInstrument(resourceString, true, false, "SelectVisa=RsVisa");
   
               string idn = instr.QueryString("*IDN?");
               Console.WriteLine("\nHello, I am: " + idn);
               Console.WriteLine("\nI am using VISA from " + instr.Identification.VisaManufacturer);
   
               Console.WriteLine("\nPress any key ...");
               Console.ReadKey();
   
               // Close the session
               instr.Dispose();
           }
       }
   }